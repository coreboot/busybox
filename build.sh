#!/bin/sh

ROOTFS=rootfs
INITRAMFS=initramfs
OUT=out

make -j $(nproc)
mkdir -p ${ROOTFS}

rm -rf ${ROOTFS}
mkdir -p ${ROOTFS}

make CONFIG_PREFIX=${ROOTFS} install

mkdir -p ${ROOTFS}/etc/init.d/

echo "#!/bin/busybox sh
mount -t proc none /proc
mount -t sysfs none /sys
mkdir /dev/pts
mount -t devpts devpts /dev/pts
# Needs CONFIG_UEVENT_HELPER
echo /sbin/mdev > /proc/sys/kernel/hotplug
/sbin/mdev -s" > ${ROOTFS}/etc/init.d/rcDev

chmod +x ${ROOTFS}/etc/init.d/rcDev

echo "#!/bin/sh
/usr/sbin/ifplugd
/sbin/udhcpc
" > ${ROOTFS}/etc/init.d/rcNet
chmod +x ${ROOTFS}/etc/init.d/rcNet

mkdir -p ${ROOTFS}/etc/
echo "::sysinit:/etc/init.d/rcDev
::sysinit:/etc/init.d/rcNet
::respawn:/usr/bin/env PS1=\"BBoxSh> \" /bin/sh +m
::ctrlaltdel:/sbin/reboot
::shutdown:/sbin/swapoff -a
::shutdown:/bin/umount -a -r
::restart:/sbin/init
ttyS0::once:/usr/bin/env PS1=\"BBoxSh> \" /sbin/getty -L ttyS0 115200 vt100 -n -l /bin/sh" > ${ROOTFS}/etc/inittab

( cd ${ROOTFS}; ln -s /sbin/init init )

rm -rf ${INITRAMFS}

mkdir -p ${INITRAMFS}/{bin,etc,home,mnt,proc,sys,usr}

cp -r ${ROOTFS}/* ${INITRAMFS}/

rm -rf ${OUT}
mkdir -p ${OUT}

( cd ${INITRAMFS}; find . -print0 | cpio --null -ov --format=newc > ../out/initramfs.cpio )
( cd ${OUT}; gzip -f -k -9 initramfs.cpio )
( cd ${OUT}; xz -f -k -e --check=crc32 --lzma2=dict=512KiB initramfs.cpio )
